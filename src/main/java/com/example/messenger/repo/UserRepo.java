package com.example.messenger.repo;

import com.example.messenger.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {
    public User findUserById(Long id);
    public void deleteById(Long id);
    public boolean existsUserByEmail(String email);
}
