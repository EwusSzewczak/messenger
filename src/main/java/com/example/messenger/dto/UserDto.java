package com.example.messenger.dto;

import com.example.messenger.entity.Message;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDto {
    Long id;
    String email;
    String imie;
    String nazwisko;
    String haslo;
    String awatar;


}
