package com.example.messenger.serwis;

import com.example.messenger.dto.MessageDto;
import com.example.messenger.entity.Message;
import com.example.messenger.entity.User;
import com.example.messenger.repo.MessageRepo;
import com.example.messenger.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageSerwis {
    @Autowired
    private MessageRepo messageRepo;

    /*public Message saveMessage(MessageDto messageDto){
        Message message = new Message();
        message.setTresc(messageDto.getTresc());
        message.setData(messageDto.getData());
        message.setUser_message(messageDto.getUser_message());
        messageRepo.save(message);
        return message;
    }*/
    public Message saveMessage(Message message1){
        Message message = new Message();
        message.setTresc(message1.getTresc());
        message.setData(message1.getData());
        //message.setUser_message(message1.getUser_message());
        messageRepo.save(message);
        return message;
    }

    public List<Message> findAll(){
        return messageRepo.findAll();
    }

}
