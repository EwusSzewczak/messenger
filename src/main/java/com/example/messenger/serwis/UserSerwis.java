package com.example.messenger.serwis;

import com.example.messenger.dto.UserDto;
import com.example.messenger.entity.User;
import com.example.messenger.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserSerwis {
    @Autowired
    private UserRepo userRepo;

    public User saveUser(UserDto newUser){
        User u=new User();
        u.setImie(newUser.getImie());
        u.setAwatar(newUser.getAwatar());
        u.setNazwisko(newUser.getNazwisko());
        u.setHaslo(newUser.getHaslo());
        u.setEmail(newUser.getEmail());
        userRepo.save(u);
        return u;
    }

    public List<User> findAll(){
        return userRepo.findAll();
    }

    public User getUser(Long id){
        return userRepo.findUserById(id);
    }

    public void deleteUserById(Long id){
        userRepo.deleteById(id);
    }

    public boolean getUserByEmail(String email){
        if(userRepo.existsUserByEmail(email))
        {
            return true;
        }
        else {
            return false;
        }
    }

}
