package com.example.messenger.controller;

import com.example.messenger.dto.MessageDto;
import com.example.messenger.entity.Message;
import com.example.messenger.entity.User;
import com.example.messenger.repo.MessageRepo;
import com.example.messenger.serwis.MessageSerwis;
import com.example.messenger.serwis.UserSerwis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController

public class MessageConstroller {
    @Autowired
    private MessageSerwis messageSerwis;

    @PostMapping("/message")
    ResponseEntity <Message> sendMessage(@RequestBody MessageDto messageDto){
        Message message = new Message();
        message.setTresc(messageDto.getTresc());
        message.setData(messageDto.getData());
        UserSerwis userSerwis = new UserSerwis();
        User user = userSerwis.getUser(messageDto.getUserId());
        //message.setUser_message(user);
        return ResponseEntity.ok(messageSerwis.saveMessage(message));
    }

    @GetMapping("/message")
    ResponseEntity <List<Message>> getMessages(){
        return ResponseEntity.ok(messageSerwis.findAll());
    }



}
