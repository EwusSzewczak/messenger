package com.example.messenger.controller;

import com.example.messenger.dto.UserDto;
import com.example.messenger.entity.User;
import com.example.messenger.repo.UserRepo;
import com.example.messenger.serwis.UserSerwis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@CrossOrigin(origins = "http://localhost:4200")
public class UserController {
    /*private final UserRepo userRepo;

    public UserController(UserRepo userRepo) {
        this.userRepo=userRepo;
    }*/
    @Autowired
    private UserSerwis userSerwis;

    @GetMapping("/uzytkownik")
    ResponseEntity <List<User>> all(){
        //return userRepo.findAll();

        List<User> listUsers = userSerwis.findAll();
        if (listUsers == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (listUsers.isEmpty()){
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(listUsers);
    }
    @PostMapping("/uzytkownik")
    ResponseEntity<User> newUser(@RequestBody UserDto newUser){
        return ResponseEntity.ok(userSerwis.saveUser(newUser));
        //return userRepo.save(u);
    }

    @GetMapping("/uzytkownik/{id}")
    ResponseEntity<User> getUser(@PathVariable Long id){
        return ResponseEntity.ok(userSerwis.getUser(id));
    }

    @DeleteMapping("/delete/{id}")
    ResponseEntity<Long> deleteUser(@PathVariable Long id){
        userSerwis.deleteUserById(id);
        return ResponseEntity.ok(id);
    }

    @GetMapping("/uzytkownik_email/{email}")
    ResponseEntity<Boolean>getUserByEmail(@PathVariable String email){
        return ResponseEntity.ok(userSerwis.getUserByEmail(email));
    }


}
