package com.example.messenger.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String tresc;
    Date data;
    @ManyToOne
    private User user_message;

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", tresc='" + tresc + '\'' +
                ", data=" + data +
                '}';
    }
}
